package com.cybertechdroid.interfaces;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.GridView;

import java.util.ArrayList;
import java.util.List;


public class ActivityGridView extends ActionBarActivity {

  GridView personasGridView=null;
  AdapterGridPersona personaAdapterGridPersona=null;
  List<Persona> personas=null;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_grid_view);

    personasGridView = (GridView) findViewById(R.id.personasGridView);
    personas = new ArrayList<>();
    for(int indice=0;indice<=10;indice++){
      personas.add(new Persona(indice,"Juan","Mejia"));
    }
    personaAdapterGridPersona = new AdapterGridPersona(getBaseContext(),R.layout.item_persona_gridview,personas);
    personasGridView.setAdapter(personaAdapterGridPersona);
  }


  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.menu_activity_grid_view, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    int id = item.getItemId();

    //noinspection SimplifiableIfStatement
    if (id == R.id.action_settings) {
      return true;
    }

    return super.onOptionsItemSelected(item);
  }
}
