package com.cybertechdroid.interfaces;

/**
 * Created by tazmaniator on 25/01/15.
 */
public class Persona {

  private int id=0;
  private String nombre=null;
  private String apellidos=null;
  private int edad=0;

  public Persona(){

  }

  public Persona(int id, String nombre){
    this.id=id;
    this.nombre=nombre;
  }

  public Persona(int id, String nombre,String apellidos){
    this.id=id;
    this.nombre=nombre;
    this.apellidos=apellidos;
  }

  public Persona(int id, String nombre, String apellidos,int edad){
    this.id=id;
    this.nombre=nombre;
    this.apellidos=apellidos;
    this.edad=edad;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getNombre() {
    return nombre;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  public String getApellidos() {
    return apellidos;
  }

  public void setApellidos(String apellidos) {
    this.apellidos = apellidos;
  }

  public int getEdad() {
    return edad;
  }

  public void setEdad(int edad) {
    this.edad = edad;
  }

  @Override
  public String toString() {
    return this.nombre;
  }

  @Override
  public int hashCode() {
    return this.id+this.nombre.hashCode()*5;
  }
}
