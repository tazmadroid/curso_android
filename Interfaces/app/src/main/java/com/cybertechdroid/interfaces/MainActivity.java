package com.cybertechdroid.interfaces;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;


public class MainActivity extends ActionBarActivity {

  private Button framelayoutButton =null;
  private Button linearlayoutHorizontalButton=null;
  private Button linearLayoutVerticalButton=null;
  private Button relativelayoutButton=null;
  private Button tablelayoutButton=null;
  private Button listasButton=null;
  private Button listasPersonalizadas=null;
  private Button gridViewButton=null;
  private Button controlesButton=null;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    framelayoutButton = (Button) findViewById(R.id.frameLayoutButton);
    framelayoutButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent framelayoutIntent = new Intent(MainActivity.this,ActivityFrameLayout.class);
        startActivity(framelayoutIntent);
      }
    });
    linearlayoutHorizontalButton=(Button) findViewById(R.id.linearlayoutHorizontalButton);
    linearlayoutHorizontalButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent linearlayoutHorizontalIntent = new Intent(MainActivity.this,ActivityLinearLayoutHorizontal.class);
        startActivity(linearlayoutHorizontalIntent);
      }
    });
    linearLayoutVerticalButton=(Button) findViewById(R.id.linearlayoutVerticalButton);
    linearLayoutVerticalButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent linearLayoutVerticalIntent=new Intent(MainActivity.this,ActivityLinearLayout.class);
        startActivity(linearLayoutVerticalIntent);
      }
    });
    relativelayoutButton=(Button) findViewById(R.id.relativelayoutButton);
    relativelayoutButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent relativelayoutIntent=new Intent(MainActivity.this,ActivityRelativeLayout.class);
        startActivity(relativelayoutIntent);
      }
    });
    tablelayoutButton = (Button) findViewById(R.id.tablelayoutButton);
    tablelayoutButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
       Intent tablelayoutIntent=new Intent(MainActivity.this,ActivityTableLayout.class);
        startActivity(tablelayoutIntent);
      }
    });
    listasButton=(Button)findViewById(R.id.listasButton);
    listasButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent listasIntent=new Intent(MainActivity.this,ActivityListas.class);
        startActivity(listasIntent);
      }
    });
    listasPersonalizadas=(Button)findViewById(R.id.listasPersonalizadasButton);
    listasPersonalizadas.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent listasPersonalizadasIntent=new Intent(MainActivity.this,ActivityListasPersonalizadas.class);
        startActivity(listasPersonalizadasIntent);
      }
    });
    gridViewButton=(Button)findViewById(R.id.gridViewButton);
    gridViewButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent gridViewIntent=new Intent(MainActivity.this,ActivityGridView.class);
        startActivity(gridViewIntent);
      }
    });
    controlesButton=(Button) findViewById(R.id.controlesButton);
    controlesButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent controlesIntent = new Intent(MainActivity.this,ActivityControles.class);
        startActivity(controlesIntent);
      }
    });
  }


  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.menu_main, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    int id = item.getItemId();

    //noinspection SimplifiableIfStatement
    if (id == R.id.action_settings) {
      return true;
    }

    return super.onOptionsItemSelected(item);
  }
}
