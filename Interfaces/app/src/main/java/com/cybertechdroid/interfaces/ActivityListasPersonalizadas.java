package com.cybertechdroid.interfaces;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;


public class ActivityListasPersonalizadas extends ActionBarActivity {

  private AdapterPersona personaAdapterPersona=null;
  private ListView personaListViewPersonalizada=null;
  private List<Persona> personas=null;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_listas_personalizadas);

    personaListViewPersonalizada = (ListView) findViewById(R.id.personasListViewPersonalizada);
    personas = new ArrayList<>();
    for(int indice=0;indice<=10;indice++){
      personas.add(new Persona(indice,"Juan","Mejia"));
    }
    personaAdapterPersona = new AdapterPersona(getBaseContext(),R.layout.item_persona,personas);
    personaListViewPersonalizada.setAdapter(personaAdapterPersona);
  }


  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.menu_activity_listas_personalizadas, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    int id = item.getItemId();

    //noinspection SimplifiableIfStatement
    if (id == R.id.action_settings) {
      return true;
    }

    return super.onOptionsItemSelected(item);
  }
}
