package com.cybertechdroid.interfaces;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by tazmaniator on 25/01/15.
 */
public class AdapterPersona extends ArrayAdapter<Persona> {

  private List<Persona> personas = null;
  private Context context=null;

  public AdapterPersona(Context context, int resource, List<Persona> objects) {
    super(context, resource, objects);
    this.personas=objects;
    this.context=context;
  }

  @Override
  public int getCount() {
    return personas.size();
  }

  @Override
  public Persona getItem(int position) {
    return personas.get(position);
  }

  @Override
  public long getItemId(int position) {
    return personas.get(position).getId();
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    View item=null;
    if (item==null){
      LayoutInflater inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
      item=inflater.inflate(R.layout.item_persona,parent,false);
    }else{
      item=convertView;
    }

    TextView nombreItemTexttView = (TextView) item.findViewById(R.id.nombreItemTextView);
    nombreItemTexttView.setText(personas.get(position).getNombre());

    TextView apellidoItemTextView = (TextView) item.findViewById(R.id.apellidoItemTextView);
    apellidoItemTextView.setText(personas.get(position).getApellidos());

    return item;
  }
}
